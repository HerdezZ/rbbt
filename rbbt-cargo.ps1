[CmdletBinding()]
param()

$ErrorActionPreference = 'Stop'

function shell {
    if ($Args.Count -eq 0) {
        throw 'Must supply at least one argument'
    }

    $command = $Args[0]
    $commandArgs = @()
    if ($Args.Count -gt 1) {
        $commandArgs = $Args[1..($Args.Count - 1)]
    }

    & $command $commandArgs
    $exitStatus = $LastExitCode
    if ($exitStatus -ne 0) {
        throw "$command $commandArgs failed with exit status $exitStatus"
    }
}

function swallow {
    param(
        [Parameter(Mandatory=$true)]
        [ScriptBlock] $ScriptBlock
    )

    $saved = $script:ErrorActionPreference
    $script:ErrorActionPreference = 'Continue'
    $result = & $ScriptBlock
    $script:ErrorActionPreference = $saved
    $result
}

class VersionInfo {
    [string] $FullVersion
    [int] $Major
    [int] $Minor
    [object] $Patch
    [int] $Offset
    [string] $Hash
    [bool] $Dirty
    [string] $CargoVersion
}

function renderTemplates {
    [OutputType([void])]
    param(
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $ProjectDir,
        [Parameter(Mandatory=$true)]
        [VersionInfo] $VersionInfo
    )

    $templateDir = Resolve-Path -Path $projectDir\_rbbt_templates
    Get-ChildItem -Path $templateDir -File -Force | ForEach-Object {
        renderTemplate `
            -ProjectDir $ProjectDir `
            -TemplateDir $templateDir `
            -TemplatePath (Resolve-Path -Path $_.FullName) `
            -VersionInfo $VersionInfo
    }
}

# Workaround for AppVeyor issue:
# Method invocation failed because [System.IO.Path] does not contain a method name 'GetRelativePath'.
# https://github.com/dotnet/sdk/issues/9414
function getRelativePath {
    [OutputType([string])]
    param(
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $RelativeTo,
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $Path
    )

    #$([System.IO.Path]::GetRelativePath($RelativeTo, $Path))"

    $relativeToStr = $RelativeTo.Path
    $pathStr = $Path.Path
    if ($pathStr.StartsWith($relativeToStr)) {
        $pathStr.Substring($relativeToStr.Length + 1)
    } else {
        throw 'Only prefix paths currently supported'
    }
}

function renderTemplate {
    [OutputType([void])]
    param(
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $ProjectDir,
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $TemplateDir,
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $TemplatePath,
        [Parameter(Mandatory=$true)]
        [VersionInfo] $VersionInfo
    )

    $relativePath = getRelativePath -RelativeTo $templateDir -Path $templatePath
    $outputPath = Join-Path -Path $projectDir -ChildPath $relativePath

    $content = Get-Content -Path $TemplatePath -Raw
    $content = $content.Replace('$full_version', $VersionInfo.FullVersion)
    $content = $content.Replace('$cargo_version', $VersionInfo.CargoVersion)
    $content = $content.Replace('$major', $VersionInfo.Major)
    $content = $content.Replace('$minor', $VersionInfo.Minor)
    $content = $content.Replace('$patch', $VersionInfo.Patch)

    $tempPath = New-TemporaryFile
    try {
        $content | Out-File -FilePath $tempPath -Encoding ASCII -NoNewline
        if (-not (Test-Path -Path $outputPath)) {
            Move-Item -Path $tempPath -Destination $outputPath
            Write-Host "Generated $outputPath"
        } elseif (-not (areFilesEqual -TempPath $tempPath -OutputPath $outputPath)) {
            Move-Item -Force -Path $tempPath -Destination $outputPath
            Write-Host "Updated $outputPath"
        }
    } finally {
        Remove-Item -ErrorAction Ignore -Path $tempPath
    }
}

function areFilesEqual {
    [OutputType([bool])]
    param(
        [Parameter(Mandatory=$true)]
        [System.IO.FileInfo] $TempPath,
        [Parameter(Mandatory=$true)]
        [string] $OutputPath
    )

    (Get-Content -Path $TempPath -Raw) -eq (Get-Content -Path $OutputPath -Raw)
}

function getVersionInfo {
    [OutputType([VersionInfo])]
    param(
        [Parameter(Mandatory=$true)]
        [System.Management.Automation.PathInfo] $GitDir
    )

    $exitStatus = 1

    if ($exitStatus -ne 0) {
        $describeStr = swallow {
            git --git-dir $GitDir describe --long --dirty --match='v[0-9]*\.[0-9]*\.[0-9]*' --exact-match 2>$null
        }
        $exitStatus = $LastExitCode
        if ($exitStatus -eq 0) {
            $exactTag = $true
        }
    }

    if ($exitStatus -ne 0) {
        $describeStr = swallow {
            git --git-dir $GitDir describe --long --dirty --match='v[0-9]*\.[0-9]*\.[0-9]*' --candidates=1 2>$null
        }
        $exitStatus = $LastExitCode
        if ($exitStatus -eq 0) {
            $exactTag = $false
        }
    }

    if (($exitStatus -eq 0) -and ($describeStr -match '^v([0-9]+)\.([0-9]+)\.([0-9]+)-([0-9]+)-(g[0-9a-f]+)(-dirty)?$')) {
        $fullVersion = $describeStr
        $major = $Matches.1
        $minor = $Matches.2
        $patch = $Matches.3
        $offset = $Matches.4
        $hash = ($Matches.5).Substring(1)
        if ($Matches.Count -eq 7) {
            $dirty = $true
        } elseif ($Matches.Count -eq 6) {
            $dirty = $false
        } else {
            throw "Unsupported Git tag format $describeStr"
        }
        $cargoVersion = "$major.$minor.$patch"
        return [VersionInfo]@{
            FullVersion = $fullVersion
            Major = $major
            Minor = $minor
            Patch = $patch
            Offset = $offset
            Hash = $hash
            Dirty = $dirty
            CargoVersion = $cargoVersion
        }
    }

    if ($exitStatus -ne 0) {
        $describeStr = swallow {
            git --git-dir $GitDir describe --long --dirty --match='v[0-9]*\.[0-9]*' --exact-match 2>$null
        }
        $exitStatus = $LastExitCode
        if ($exitStatus -eq 0) {
            $exactTag = $true
        }
    }

    if ($exitStatus -ne 0) {
        $describeStr = swallow {
            git --git-dir $GitDir describe --long --dirty --match='v[0-9]*\.[0-9]**' --candidates=1 2>$null
        }
        $exitStatus = $LastExitCode
        if ($exitStatus -eq 0) {
            $exactTag = $false
        }
    }

    if (($exitStatus -eq 0) -and ($describeStr -match '^v([0-9]+)\.([0-9]+)-([0-9]+)-(g[0-9a-f]+)(-dirty)?$')) {
        $fullVersion = $describeStr
        $major = $Matches.1
        $minor = $Matches.2
        $patch = $null
        $offset = $Matches.3
        $hash = ($Matches.4).Substring(1)
        if ($Matches.Count -eq 6) {
            $dirty = $true
        } elseif ($Matches.Count -eq 5) {
            $dirty = $false
        } else {
            throw "Unsupported Git tag format $describeStr"
        }
        $cargoVersion = "$major.$minor.0"
        return [VersionInfo]@{
            FullVersion = $fullVersion
            Major = $major
            Minor = $minor
            Patch = $patch
            Offset = $offset
            Hash = $hash
            Dirty = $dirty
            CargoVersion = $cargoVersion
        }
    }

    $headHash = shell git --git-dir $gitDir rev-parse HEAD
    if ($describeStr.Length -eq 0) {
        throw "No Git tag available for commit $headHash"
    } else {
        throw "Unsupported Git tag format $describeStr for commit $headHash"
    }
}

function main {
    [OutputType([void])]
    param()

    $projectDir = Get-Location
    $versionInfo = getVersionInfo -GitDir (Resolve-Path -Path $projectDir\.git)
    Write-Host "Version $($versionInfo.FullVersion)"
    renderTemplates -ProjectDir $projectDir -VersionInfo $versionInfo
}

main
