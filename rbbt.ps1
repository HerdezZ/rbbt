[CmdletBinding()]
param()

$ErrorActionPreference = 'Stop'

function main {
    [OutputType([void])]
    param()

    $projectDir = Get-Location
    $projectType = (Get-Content -Path $projectDir\.rbbt -Raw).Trim()
    Invoke-Expression "$PSScriptRoot\rbbt-$projectType.ps1 $ArgumentList"
}

main
